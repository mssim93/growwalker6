//
//  Ground+CoreDataProperties.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 1..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Ground {

    @NSManaged var position: NSNumber?  //식물 위치
    @NSManaged var hasGrown: NSNumber?  //식물의 존재 여부
    @NSManaged var startDate: NSDate?   //심은 시간날짜
    @NSManaged var nutrients: NSNumber?  //영양제 적용여부

}
