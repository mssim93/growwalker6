//
//  ViewController.swift
//  GrowWalker
//
//  Created by Myongji on 2016. 6. 30..
//  Copyright © 2016년 Myongji. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet var GoldImage: UIImageView!      //tag1
    @IBOutlet weak var EnergyImage: UIImageView!    //tag2
    @IBOutlet weak var WaterImage: UIImageView!     //tag3
    
    @IBOutlet weak var GoldLabel: UILabel!      //tag1
    @IBOutlet weak var EnergyLabel: UILabel!    //tag2
    
    @IBOutlet weak var WaterProgress: UIProgressView!
    
    @IBOutlet weak var LV1Container: UIView! //tag1
    @IBOutlet weak var LV2Container: UIView! //tag2
    @IBOutlet weak var LV3Container: UIView! //tag3
    
    @IBOutlet weak var MenuButton: UIButton!        //tag1
    @IBOutlet weak var InventoryButton: UIButton!   //tag2

    
    override func viewDidLoad() {
        super.viewDidLoad()
        func showModal() {
            let modalViewController = MenuView()
            modalViewController.modalPresentationStyle = .OverCurrentContext
            presentViewController(modalViewController, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView:UITableView) -> NSInteger
    {
        let buffer = Int(arc4random() % 10);
    return 4 + buffer 
    }

    
}

