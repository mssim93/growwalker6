//
//  AchievementList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Require Attributes :
//  @NSManaged var no: NSNumber?
//  @NSManaged var title: String?
//  @NSManaged var body: String?
//  @NSManaged var rewardGold: NSNumber?
//  @NSManaged var rewardEnergy: NSNumber?
//  @NSManaged var rewardItems: String?
//  @NSManaged var achieved: NSNumber?
//

import Foundation


class AchievementList {
    let formatter = NSNumberFormatter()
    var AchievementDict = [Int: Array<String>]()
    var dictionary = [String: String]()
    var itemArray:[[String: String]] = [
        ["body":"","title":"","prev":"0","gold":"","items":"","energy":"","current":"0","target":"0","criteria":""],
        ["body":"처음 식물을 심었다","title":"첫걸음","prev":"0","gold":"1000","items":"","energy":"","current":"0","target":"1","criteria":"plants"],
        ["body":"처음 식물을 수확했다","title":"첫 수확","prev":"0","gold":"1000","items":"","energy":"100","current":"0","target":"1","criteria":"plants"]
    ]
    func getArray() -> [[String: String]] {
        return itemArray
    }
    
}