import UIKit

class CollectionViewController: UICollectionViewController{
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
class CardViewController: UIViewController{
    
    @IBOutlet weak var Card: UIImageView!
    @IBOutlet weak var PlantName: UILabel!
    @IBOutlet weak var PlantType: UILabel!
    @IBOutlet weak var PlantInfo: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}